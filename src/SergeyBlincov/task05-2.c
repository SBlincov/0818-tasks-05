#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>
#include <math.h>

#define MAX 256

typedef struct
{
	int  count_symbols;
	int  count_word;
	int  count_sign;
	int  count_numbers;
	int  midlword;
	unsigned char popular_symbol;
} STATIK;

int CountWordStr(const char *str)
{
	char inWord = 1;
	int count = 0;
	while (*str)
	{
		if ((*str != ' ') && inWord && *str)
		{
			inWord = 0;
			count++;
		}
		if (*str == ' ')
			inWord = 1;
		str++;
	}
	return count;
}

int CountSignWord(const char *str)
{
	int count = 0;
	while (*str)
	{
		if ((*str == '.') || (*str == ',') || (*str == '!') || (*str == '?'))
			count++;
		str++;
	}
	return count;
}

int CountNumbersWord(const char *str)
{
	int count = 0;
	while (*str)
	{
		if ((*str >= '0') && (*str <= '9'))
			count++;
		str++;
	}
	return count;
}

int LengWords(char *str)
{
	char inWord = 1;
	int count = 0;
	char *StartWord;
	while (*str)
	{
		if ((*str != ' ') && inWord && *str)
		{
			inWord = 0;
			StartWord = str;
			while (*StartWord && *StartWord != ' ')
			{
				count++;
				StartWord++;
			}
		}
		else
		if (*str == ' ')
			inWord = 1;
		str++;
	}
	return count;
}

void PrintfStatic(STATIK * Info)
{
	printf("the number of symbols - %d\n", Info->count_symbols);
	printf("the number of words - %d\n", Info->count_word);
	printf("number of punctuation marks - %d\n", Info->count_sign);
	printf("number of digits - %d\n", Info->count_numbers);
	printf("the average length of a word - %d\n", Info->midlword);
	printf("the most popular character - '%c'\n", Info->popular_symbol);
}

int main(int argc, char* argv[])
{
	STATIK Info;
	FILE *fp;
	int count = 0, i = 0, max = 0, leng;
	char buf[MAX];
	char **data = NULL;
	int tmp[MAX] = { 0 };
	setlocale(LC_ALL, "rus");
	if (argc < 2)
	{
		puts("Invalid parameter transfer in main");
		exit(-1);
	}
	printf("%d\n%s\n", argc, argv[1]);
	fp = fopen(argv[1], "rt");
	if (fp == NULL)
	{
		puts("File not found");
		exit(1);
	}
	while (fgets(buf, MAX, fp))
		count++;
	rewind(fp);
	data = (char **)malloc(count*sizeof(char*));
	while (fgets(buf, MAX, fp))
	{
		leng = strlen(buf);
		if (leng >= MAX) exit(2);
		buf[leng - 1] = 0;
		data[i] = (char*)malloc((strlen(buf) + 1) *sizeof (char));
		strcpy(data[i], buf);
		i++;
	}

	Info.count_symbols = 0;
	for (i = 0; i<count; i++)
		Info.count_symbols = Info.count_symbols + strlen(data[i]);


	Info.count_word = 0;
	for (i = 0; i<count; i++)
		Info.count_word += CountWordStr(data[i]);


	Info.count_sign = 0;
	for (i = 0; i<count; i++)
		Info.count_sign += CountSignWord(data[i]);


	Info.count_numbers = 0;
	for (i = 0; i<count; i++)
		Info.count_numbers += CountNumbersWord(data[i]);


	Info.midlword = 0;
	for (i = 0; i<count; i++)
		Info.midlword += LengWords(data[i]);

	Info.midlword = floorf((float)Info.midlword / Info.count_word + 0.5f);



	for (i = 0; i<count; i++)
	while (*data[i])
		tmp[(unsigned char)*data[i]++]++;

	for (i = 0; i<MAX; i++)
	if (tmp[i]>max)
	{
		max = tmp[i];
		Info.popular_symbol = i;
	}

	PrintfStatic(&Info);

	for (i = 0; i<count; i++)
		free(data[i]);
	free(data);

}
